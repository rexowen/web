import React from 'react';
import '../../App.css';
import { Button } from '../Button/Button.js';
import './HomePage.css';

function HeroSection() {
  return (
    <div className='hero-container'>
      <h1>Welcome to eVengerShop</h1>
      <p>Check out our pc games!</p>
      <div className='hero-btns'>
        <Button
          className='btn'
          buttonStyle='btn--outline'
          buttonSize='btn--large'
        >
          Buy Now
        </Button>
      </div>
    </div>
  );
}

export default HeroSection;
