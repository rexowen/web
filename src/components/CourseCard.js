
import { Card } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './Cards/Cards.css';
import CardItem from './Cards/CardItem';
import Valorant from '../images/valorant.jpg';

export default function ProductCard({productProp}) {

	const { _id, name } = productProp;

	return(
		<div className='cards'>
		<div className='cards__wrapper'>
						<ul className='cards__items'>
							<CardItem
							  src={Valorant}
							  text={name}
							  path={`/products/${_id}`}
							/>
				</ul>
				</div>
				</div>
		)
}


//Check if the CourseCard Component is getting the correct prop types
//Proptypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next
ProductCard.propTypes = {
	//The 'shape' method is used to check if a prop object conforms to a specific 'shape'
	productProp: PropTypes.shape({
		//define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
