import React from 'react';
import './Cards.css';
import CardItem from './CardItem';
import Valorant from '../../images/valorant.jpg';
import Darkness from '../../images/darkness.jpg';
import back4blood from '../../images/back4blood.jpg';
import NewWorld from '../../images/newworld.jpg';
import r6 from '../../images/r6.jpg';

function Cards() {
  return (
    <div className='cards'>
      <h1>Check out our EPIC PC GAMES!</h1>
      <div className='cards__container'>
        <div className='cards__wrapper'>
          <ul className='cards__items'>
            <CardItem
              src={Valorant}
              text='Riot Games presents VALORANT: a 5vs5 character-based tactical FPS where precise gunplay meets unique agent abilities'
              label='Tactical FPS Game'
              path='/products'
            />
            <CardItem
              src={Darkness}
              text='FROM THE DARKNESS is a first-person horror game that focuses on creating fear in the player.'
              label='First-Person Horror Game'
              path='/products'
            />
          </ul>
          <ul className='cards__items'>
            <CardItem
              src={back4blood}
              text='BACK 4 Blood is a thrilling cooperative first-person shooter from the creators of the critically acclaimed Left 4 Dead franchise.'
              label='Zombie First-Person Shooter'
              path='/products'
            />
            <CardItem
              src={NewWorld}
              text='In NEW WORLD, explore a thrilling, open-world MMO filled with danger and opportunity where you will forge a new destiny for yourself as an adventurer shipwrecked on the supernatural island of Aeternum. '
              label='MMO'
              path='/products'
            />
            <CardItem
              src={r6}
              text='Master the art of destruction and gadgetry in Tom Clancy’s Rainbow Six Siege. Face intense close quarters combat, high lethality, tactical decision making, team play and explosive action within every moment.'
              label='Tactical First-Person Shooter'
              path='/products'
            />
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Cards;