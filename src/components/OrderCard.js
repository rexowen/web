const [blogs, setBlogs] = useState(null)

  useEffect(() => {
    fetch('http://localhost:4000/users/myOrders')
      .then(res => {
        return res.json();
      })
      .then(data => {
        setBlogs(data);
      })
  }, [])

  return (
    <div className="home">
      {blogs && <BlogList blogs={blogs} />}
    </div>
  );
}