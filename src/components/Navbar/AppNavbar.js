import React, { useState, useEffect, Fragment, useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';

import UserContext from '../../UserContext';
import './Navbar.css';

export default function AppNavbar(){

	const [click, setClick] = useState(false);
  const [button, setButton] = useState(true);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

    const showButton = () => {
      if (window.innerWidth <= 960) {
        setButton(false);
      } else {
        setButton(true);
      }
    };

    useEffect(() => {
      showButton();
    }, []);

    window.addEventListener('resize', showButton);

    const { user } = useContext(UserContext)

    let leftNav = (user.accessToken !== null) ?
      <Fragment>
        <Link className='nav-links' as={NavLink}  to="/logout">Logout</Link>
      </Fragment>
      :
      <li className='nav-item'>
        <Link as={NavLink}   className='nav-links' onClick={closeMobileMenu}
     to="/login">Log In</Link>
        <Link as={NavLink}  className='nav-links' onClick={closeMobileMenu}
    to="/register">Register</Link>
      </li>



	return(
    <>
          <nav className='navbar'>
            <div className='navbar-container'>
              <Link to='/' className='navbar-logo' onClick={closeMobileMenu}>
                eVengerShop
              </Link>
              <div className='menu-icon' onClick={handleClick}>
                <i className={click ? 'fas fa-times' : 'fas fa-bars'} />
              </div>
              <ul className={click ? 'nav-menu active' : 'nav-menu'}>
                <li className='nav-item'>
                  <Link
                    as={NavLink}
                    to="/products"
                    className='nav-links'
                    onClick={closeMobileMenu}
                  >
                    Store
                  </Link>
                </li>
                <li>
                    {leftNav}

                </li>
                <li>
            </li>
          </ul>
        </div>
          </nav>
        </>
      );
    }


