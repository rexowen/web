import { useState, useEffect } from 'react';
import './App.css';
import AppNavbar from './components/Navbar/AppNavbar.js';
import Home from './pages/Home';
import Store from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Orders from './pages/myOrders';
import SpecificProduct from './pages/SpecificProduct';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import { UserProvider } from './UserContext';



function App() {
  const [user, setUser] = useState({ 
              accessToken: localStorage.getItem('accessToken'),
              email: localStorage.getItem('email'),
              isAdmin: localStorage.getItem('isAdmin') === 'true'
            })
  const unsetUser = () => {
        localStorage.clear()
  }
  useEffect(()=>{
      console.log(user);
      console.log(localStorage)
  }, [user])
  return (
    <>
    <UserProvider value={ { user, setUser, unsetUser } }>
        <Router>
          <AppNavbar />
            <Switch>
              < Route exact path="/" component={Home} />
              < Route exact path="/products" component={Store} />
              < Route exact path="/myOrders" component={Orders} />
              < Route exact path="/products/:productId" component={SpecificProduct}/>
              < Route exact path="/register" component={Register} />
              < Route exact path="/login" component={Login} />
              < Route exact path="/logout" component={Logout} />
              <Route component={Error} />
            </Switch>
        </Router>
    </UserProvider>
    </>
  );
}


export default App;