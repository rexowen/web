import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Link, useParams } from 'react-router-dom';
import CardItem from '../components/Cards/CardItem';
import Valorant from '../images/valorant.jpg';
import '../components/Cards/Cards.css';

import UserContext from '../UserContext';


export default function SpecificProduct(){

	const { user } = useContext(UserContext);

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState(0)

	//useParams() contains any values we are trying to pass in the URL stored in a key/value pair
	//useParams is how we receive the courseId passed via the URL

		const { productId } = useParams();

	useEffect(() =>{
		fetch(`http://localhost:4000/products/${ productId }`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, []) // eslint-disable-line react-hooks/exhaustive-deps


	const enroll = (productId) =>{
		fetch('http://localhost:4000/users/checkout', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				Swal.fire({
					title: "Yeeeeeeey!",
					icon: 'success',
					text: `You have successfully enrolled for this ${ name } item.`
				})
			}else{
				Swal.fire({
					title: "OOooooops!",
					icon: 'error',
					text: `Something went wrong. Please try again`
				})
			}
		})
	}

	return(
		<div className='cards'>
			<h1>Price: Php {price}</h1>
			<div className='cards__container'>
								<ul className='cards__items'>
									<CardItem
									  src={Valorant}
									  text={description}
									  label={name}
									/>
						</ul>
					{
						user.accessToken !== null ?
							<Button className='btn'
          buttonStyle='btn--outline'
          buttonSize='btn--large' block onClick={() => enroll(productId)}>Purchase</Button>
							:
							<Link className='btn'
          buttonStyle='btn--outline'
          buttonSize='btn--large' to="/login">Login to Purchase
							</Link>


					}

				</div>
		</div>

		)
}
