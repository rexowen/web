import React from 'react';
import '../App.css';
import Cards from '../components/Cards/Cards.js';
import HeroSection from '../components/HomePage/HomePage';
import Footer from '../components/Footer/Footer';

function Home() {
  return (
    <>
      <HeroSection />
      <Cards />
      <Footer />
    </>
  );
}

export default Home;