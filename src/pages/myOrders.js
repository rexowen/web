import { useState, useEffect, useContext } from 'react';
import { Container, Card} from 'react-bootstrap';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';


export default function Orders(){

	const [data, setData] = useState(null);
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState(null);

	useEffect(() => {
		fetch(`http://localhost:4000/users/myOrders`)
		.then(response => {
			if (response.ok) {
				return response.json()
			}
			throw response;
		})
		.then(data => {
			setData(data);
		})
		.catch(error => {
			console.error("Error fetching data: ", error);
			setError(error);
		})
		.finally(() => {
			setLoading(false);
		})
	}, [])

	return(
		<Container>
			<Card>
				<Card.Header className="bg-dark text-white text-center pb-0">
					<h4>{data.firstName}</h4>
					<h4>{data.lastName}</h4>
					<h4>{data.email}</h4>
					<h4>{data.orders[0]}</h4>
				</Card.Header>
			</Card>
		</Container>

		)



	/*const { user } = useContext(UserContext);

	const [name, setName] = useState('')
	const [orders, setOrders] = useState('')

	//useParams() contains any values we are trying to pass in the URL stored in a key/value pair
	//useParams is how we receive the courseId passed via the URL

		const { userId } = useParams();

	useEffect(() =>{
		fetch(`http://localhost:4000/users/myOrders`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setOrders(data.orders)
		})
	}, []) // eslint-disable-line react-hooks/exhaustive-deps


	const transaction = (userId) =>{
		fetch('http://localhost:4000/users/myOrders', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			},
			body: JSON.stringify({
				userId: userId
			})
		})
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setOrders(data.orders)
		})
	}

	return(
		<Container>
			<Card>
				<Card.Header className="bg-dark text-white text-center pb-0">
					<h4>{name}</h4>
				</Card.Header>

				<Card.Body className="text-center">
					<Card.Text>
						{orders}
					</Card.Text>
				</Card.Body>

				<Card.Footer>
					{
						user.accessToken !== null ?
							<Button variant="primary" block onClick={() => transaction(userId)}>Check Purchase History</Button>
							:
							<Link className="btn btn-warning btn-block" to="/login">Log In to Check Your Purchase History
							</Link>


					}

				</Card.Footer>

			</Card>
		</Container>

		)*/
}
